package com.indra.franh20.ProyectoKafkaInicial.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class MessageConsumer {

    private static final Logger log = LoggerFactory.getLogger(MessageConsumer.class);

    @KafkaListener(topics = "my-topic", groupId = "franh21-group")
    public void listen(String message) {
        log.info("Mensaje recibido: " + message);
    }

}