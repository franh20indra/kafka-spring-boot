package com.indra.franh20.ProyectoKafkaInicial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoKafkaInicialApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoKafkaInicialApplication.class, args);
	}

}
